console.log("Renreder login.js");

const BrowserWindow = require ('electron').remote.BrowserWindow;
const path = require ('path');
const url = require ('url');
const ipc = require('electron').ipcRenderer;


const loginBtn = document.getElementById('login');
loginBtn.addEventListener('click' , (event)=>{
    event.preventDefault();

    //TODO:Checkear conexion base de datos con el usuario devuelto 
    //TODO:cerrar ventana login si es correcto si no advertir del error activando un div.

    //Es Correcto, entonces crear la ventana del program y cerrar la ventana de login
    //ventana principal
    
    let programWindow = new BrowserWindow({
        fullscreenWindowTitle : false,
        fullscreen:true
    });
    //cargamos la pagina principal de nuestro programa
    programWindow.loadURL( url.format({
        pathname: path.join(__dirname , './views/program.html'),
        protocol : 'file',
        slashes:true
    }));

    // abrimos DevTools.
    //programWindow.webContents.openDevTools(); 
    
    programWindow.on('closed' , ()=>{
        programWindow = null;
    });

    ipc.send('close-login');
    ipc.on('closed-login-window' , (event, arg)=>{
        console.log(arg);
    });

});


