console.log("Main process - login.js");

//con app crearemos nuestra appliacion principal el cual le pasaremos 
//una ventana formada con el objeto BrowserWindow
const { app, BrowserWindow } = require('electron');
const path = require('path');
const url = require('url');
let loginWindown;
const ipc = require('electron').ipcMain;
const dialogBox = require('electron').dialog;
//dialogBox0.showErrorBox('');
const Menu = require('electron').Menu;
const electron = require('electron');

process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';

function createWindow() {
    //ventana login
    loginWindown = new BrowserWindow({
        width: 800,
        height: 600,
        center: true,
        alwaysOnTop: true,
        frame: false,
        transparent: true
    });
    //cargamos la pagina principal de nuestro programa
    loginWindown.loadURL(url.format({
        pathname: path.join(__dirname, './app/views/login.html'),
        protocol: 'file',
        slashes: true
    }));
    // abrimos DevTools.
    loginWindown.webContents.openDevTools();

    loginWindown.on('closed', () => {
        loginWindown = null;
    });
}

//Este evento ocurre cuando esta inicializado electron,
//entonces ejecutamos la venta
app.on('ready', () => {
    createWindow();
    var template = [
        {
            label: 'electron',
            submenu: [
                {
                    label: 'electron 1',
                    click: () => { console.log('Click electron 1'); }
                },
                {
                    type: 'separator'
                },
                {
                    label: 'electron 2',
                    click: () => { console.log('Click electron 2'); }
                },
                {
                    label: 'help',
                    click: () => {
                        electron.shell.openExternal('https://electronjs.org/');
                        console.log('Click File 2');
                    }
                }
            ]
        },
        {
            label: 'File',
            submenu: [
                {
                    label: 'File 1',
                    click: () => { console.log('Click File 1'); }
                },
                {
                    label: 'File 2',
                    click: () => { console.log('Click File 2'); }
                }
            ]
        },
        {
            label: 'Edit',
            submenu:[
                {role:'undo'},
                {role:'redo'},
                {role:'cut'},
                {role:'copy'},
                {role:'paste'},
                {role:'undo'},
                {role:'delete'},
                {role:'selectall'}
            ]
        },
    ];
    const menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(menu);
});

// Evento cuando todas las ventanas has sido cerradas
app.on('window-all-closed', function () {
    //Esta parte de especificacion es de propia de electron
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

ipc.on('close-login', (event) => {
    loginWindown.close();
    event.sender.send('closed-login-window', 'Closed login!');

});
